import 'package:audioplayers/audio_cache.dart';
import 'package:flutter/material.dart';

void main() => runApp(Xylophone());

class Xylophone extends StatelessWidget {
  void playButton(int soundNumber) {
    final player = AudioCache();
    player.play('note$soundNumber.wav');
  }

  Expanded buildKey({int soundNumber, Color color, String message}) {
    return Expanded(
      child: TextButton(
        onPressed: () {
          playButton(soundNumber);
        },
        child: Text(message),
        style: TextButton.styleFrom(
          backgroundColor: color,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.black,
        // backgroundColor: Colors.red,
        appBar: AppBar(
          title: Text("Xylophone"),
        ),
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              buildKey(color: Colors.blue, soundNumber: 1, message: "1"),
              buildKey(color: Colors.indigo, soundNumber: 2, message: "2"),
              buildKey(color: Colors.green, soundNumber: 3, message: "3"),
              buildKey(color: Colors.yellow, soundNumber: 4, message: "4"),
              buildKey(color: Colors.orange, soundNumber: 5, message: "5"),
              buildKey(color: Colors.red, soundNumber: 6, message: "6"),
              buildKey(color: Colors.white, soundNumber: 7, message: "7"),
            ],
          ),
        ),
      ),
    );
  }
}
